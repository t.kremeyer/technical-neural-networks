# PA-E: Hopfield Network

## Usage

Compile the project and start it using class `Main` as entry point.  
The program does not need any command line options, everything is
configured in Main class (cf. source code for documentation).  
Supported pattern formats are:  
`#` or `+` or `1` for +1  
`_` or `-` or `0` for -1  
or a List of Booleans

## Output

The program will output the weight matrix into a file
`out/weightMatrix.csv`, delimited by tabs and newlines.  
Also, the program will output the state of the net
(current pattern or energy level) to stdout each update step or each
full time step (customizable).