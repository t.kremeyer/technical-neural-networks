import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 10.12.2017.
 */
public class Main
{
	public static int patternDimension = 100;
	public static int randomSeed = 1894712;

	//Also print state (current pattern / energy) after each neuron upadate?
	public static boolean printIntermediateState = true;

	//The patterns to be learned, c.f. parsePattern() for format
	public static String[] learnPatterns = {
			"#####_____#####_____#####_____#####_____#####_____#####_____#####_____#####_____#####_____#####_____",
			"#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_#_",
			"##__##__##__##__##__##__##__####__##__##__##__##__##__##__####__##__##__##__##__##__##__####__##__##"
	};

	//The pattern to be adapted
	public static String adaptPattern =
			"____________________________________________________________________________________________________";

	//How many timesteps to adapt the pattern
	public static int learnSteps = 5;

	//Output filename for weight matrix
	public static String filename = "out/weightMatrix.csv";


	public static void main(String[]args) throws ParseException, FileNotFoundException, UnsupportedEncodingException
	{
		HopfieldNet net = new HopfieldNet(patternDimension, randomSeed);

		for(String p : learnPatterns)
		{
			net.setPattern(parsePattern(p));
			net.learnPattern();
		}

		net.printMatrix(new PrintStream(new FileOutputStream(filename, false)));

		System.out.println("ADAPTING:\n");
		net.setPattern(parsePattern(adaptPattern));

		//SET THRESHOLDS
		//
		//Set to pattern:
		// net.setThresholdsToPattern();
		//Set to value:
		// net.setThresholds(value);
		//Set individual thresholds to values v1, v2, ...:
		// net.setThresholds(v1, v2, ...);
		net.setThresholdsToPattern();

		if(patternDimension <= 100) net.printPattern();
		else System.out.println("Energy = " + net.getEnergy());

		for(int i = 0; i < learnSteps; i++)
		{
			net.recallStep(printIntermediateState);
			if(patternDimension <= 100) net.printPattern();
			else System.out.println("Energy = " + net.getEnergy() + " [ ALL NEURONS UPDATED FOR THIS TIME STEP ] ");
		}
	}


	//Pattern string:
	// '#' or '+' or '1' => +1
	// '_' or '-' or '0' => -1
	private static List<Boolean> parsePattern(String patternString) throws ParseException
	{
		List<Boolean> pattern = new ArrayList<>();

		for(int i = 0; i < patternString.length(); i++)
		{
			char c = patternString.charAt(i);

			if(c == '#' || c == '+' || c == '1') pattern.add(true);
			else if(c == '_' || c == '-' || c == '0') pattern.add(false);
			else throw new ParseException("Unexpected character in pattern string: " + c, i);
		}

		return pattern;
	}
}
