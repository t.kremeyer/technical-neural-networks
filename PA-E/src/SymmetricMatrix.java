import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 10.12.2017.
 */
public class SymmetricMatrix
{
	List<List<Integer>> matrix;

	public SymmetricMatrix(int dim)
	{
		matrix = new ArrayList<>();

		// Initialize a lower triangular matrix (without main diagonal since this is 0)
		// Start at row 1 since first row is completely filled with 0.
		for(int row = 1; row < dim; row++)
		{
			ArrayList<Integer> w = new ArrayList<>();

			//We need i-1 fields for row i
			for(int col = 0; col < row; col++)
			{
				w.add(0);
			}
			matrix.add(w);
		}
	}

	public int getDimension()
	{
		return matrix.size()+1;
	}

	public int getField(int row, int col)
	{
		if(row == col) return 0;

		//row i has index i-1 in matrix (since row 0 is not stored)
		else if(row < col) return matrix.get(col-1).get(row);
		else return matrix.get(row-1).get(col);
	}

	public void setField(int row, int col, int val)
	{
		if(row == col) throw new IllegalArgumentException("Tried to set value on main diagonal.");

		//row i has index i-1 in matrix (since row 0 is not stored)
		else if(row < col) matrix.get(col-1).set(row, val);
		else matrix.get(row-1).set(col, val);
	}

	public void print(PrintStream out)
	{
		for(int row = 0; row < matrix.size()+1; row++)
		{
			for(int col = 0; col < matrix.size()+1; col++)
			{
				out.print(getField(row, col) + "\t");
			}
			out.print("\n");
		}
	}

}
