import java.io.PrintStream;
import java.util.*;

/**
 * Created by Tobias on 10.12.2017.
 */
public class HopfieldNet
{
	private Random rnd;
	private List<Boolean> pattern;
	private SymmetricMatrix weights;
	private List<Integer> neuronList;	//Cache for update order of neurons
	private List<Double> thresholds;

	public HopfieldNet(int patternDimension)
	{
		weights = new SymmetricMatrix(patternDimension);
		rnd = new Random();
		initNeuronList();
		initThresholdList();
	}

	public HopfieldNet(int patternDimension, long randomSeed)
	{
		weights = new SymmetricMatrix(patternDimension);
		rnd = new Random(randomSeed);
		initNeuronList();
		initThresholdList();
	}

	public void setPattern(List<Boolean> pattern)
	{
		if(pattern.size() != weights.getDimension())
			throw new IllegalArgumentException("Pattern size does not match dimension");

		this.pattern = pattern;
	}

	public void setPattern(boolean ... pattern)
	{
		if(pattern.length != weights.getDimension())
			throw new IllegalArgumentException("Pattern size does not match dimension");

		this.pattern = new ArrayList(weights.getDimension());
		for(boolean p : pattern)
		{
			this.pattern.add(p);
		}
	}

	public void setThresholds(List<Double> thresholds)
	{
		if(thresholds.size() != weights.getDimension())
			throw new IllegalArgumentException("Threshold count does not match dimension");

		this.thresholds = thresholds;
	}

	public void setThresholds(double ... thresholds)
	{
		if(thresholds.length != weights.getDimension())
			throw new IllegalArgumentException("Threshold count does not match dimension");

		for(int i = 0; i < this.thresholds.size(); i++)
		{
			this.thresholds.set(i, thresholds[i]);
		}
	}

	public void setThresholds(double threshold)
	{
		for(int i = 0; i < thresholds.size(); i++)
		{
			thresholds.set(i, threshold);
		}
	}

	public void setThresholdsToPattern()
	{
		for(int i = 0; i < thresholds.size(); i++)
		{
			thresholds.set(i, pattern.get(i) ? 1d : -1d);
		}
	}

	public void learnPattern()
	{
		for(int row = 1; row < pattern.size(); row++)
		{
			for(int col = 0; col < row; col++)
			{
				int delta = (pattern.get(row).booleanValue() == pattern.get(col).booleanValue())? 1 : -1;
				weights.setField(row, col, weights.getField(row, col) + delta);
			}
		}
	}

	public void recallStep(boolean printEachStep)
	{
		Collections.shuffle(neuronList, rnd);

		//Async update for each neuron
		for (int n : neuronList)
		{
			//Calculate feedback of neuron n
			double feedback = 0;
			for(int i = 0; i < weights.getDimension(); i++)
			{
				feedback += (pattern.get(i) ? 1 : -1) * weights.getField(i, n);
			}

			if(feedback > thresholds.get(n)) pattern.set(n, true);
			else if(feedback < thresholds.get(n)) pattern.set(n, false);

			if(printEachStep && weights.getDimension() <= 100)
				printPattern();
			else if(printEachStep)
				System.out.println("Energy = " + getEnergy() + " [ updated neuron " + n + " ]");
		}
	}

	public List<Boolean> getPattern()
	{
		return pattern;
	}

	public void printMatrix(PrintStream out)
	{
		weights.print(out);
	}

	public void printPattern()
	{
		printPattern('#', '_');
	}

	public void printPattern(char pos, char neg)
	{
		for(boolean p : pattern)
		{
			System.out.print(p ? pos : neg);
		}
		System.out.print("\n");
	}

	public double getEnergy()
	{
		double energy = 0;
		for(int row = 0; row < weights.getDimension(); row++)
		{
			for(int col = 0; col < weights.getDimension(); col++)
			{
				energy += weights.getField(row, col) * (pattern.get(row)?1:-1) * (pattern.get(col)?1:-1);
			}
		}
		energy = -0.5 * energy;

		for(int neuron = 0; neuron < pattern.size(); neuron++)
		{
			energy += (pattern.get(neuron)?1:-1) * thresholds.get(neuron);
		}

		return energy;
	}

	private void initThresholdList()
	{
		thresholds = new ArrayList<>();
		for(int i = 0; i < weights.getDimension(); i++)
		{
			thresholds.add(0d);
		}
	}

	private void initNeuronList()
	{
		neuronList = new ArrayList<>(weights.getDimension());
		for(int i = 0; i < weights.getDimension(); i++)
		{
			neuronList.add(i);
		}
	}

}
