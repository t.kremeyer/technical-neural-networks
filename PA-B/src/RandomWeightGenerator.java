import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Tobias on 05.11.2017.
 */
public class RandomWeightGenerator extends Random
{
	private double min, max;

	public RandomWeightGenerator(long seed, double min, double max)
	{
		super(seed);
		this.min = min;
		this.max = max;
	}

	public double nextWeight()
	{
		//As described in https://stackoverflow.com/a/3680666
		return min + (nextDouble() * (max - min));
	}

	public ArrayList<Double> nextWeights(int count)
	{
		ArrayList<Double> result = new ArrayList<>();

		for(int i = 0; i < count; i++)
		{
			result.add(nextWeight());
		}

		return result;
	}
}
