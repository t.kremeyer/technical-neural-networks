import TransferFunctions.LinearTransferFunction;
import TransferFunctions.LogisticTransferFunction;
import TransferFunctions.TanhTransferFunction;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Tobias on 05.11.2017.
 */
public class Main
{
	static int epochCount = 1000;
	static int inputValueCount = 2;

	static TanhTransferFunction tanh = new TanhTransferFunction();
	static LogisticTransferFunction logistic = new LogisticTransferFunction();
	static LinearTransferFunction linear = new LinearTransferFunction();

	static RandomWeightGenerator randomWeightGenerator = new RandomWeightGenerator(1234567, -2.0, 2.0);
	static Random shuffleNumberGenerator = new Random(1234532);

	public static void main(String[] args) throws IOException
	{
		if(args.length < 2)
		{
			System.err.println("Usage: [Path to input training data file] [Path to input test data file]");
			System.exit(1);
		}

		InputLayer inputLayer;
		List<Layer> layers = new ArrayList<>();

		inputLayer = new InputLayer(inputValueCount);

		Layer l1 = new Layer(5, 0.2, inputLayer, tanh, randomWeightGenerator);
		Layer l2 = new Layer(1, 0.2, l1, tanh, randomWeightGenerator);

		layers.add(l1);
		layers.add(l2);

		MLP mlp = new MLP(inputLayer, layers);

		List<TrainingExample> trainingExamples = parseTrainingExamples(args[0]);
		List<TrainingExample> testExamples = parseTrainingExamples(args[1]);

		Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("out/learning.curve"), "utf-8"));

		Collections.shuffle(trainingExamples, shuffleNumberGenerator);

		for(int i = 0; i < epochCount; i++)
		{
			for(TrainingExample example : trainingExamples)
			{
				writer.write(Double.toString(getGlobalError(mlp, trainingExamples)) + " "
				+ Double.toString(getGlobalError(mlp, testExamples)) + "\n");

				mlp.forward(example.input);
				mlp.backprop(example.teacherValues);
				mlp.updateWeights();
			}
		}

		writer.close();

	}

	private static List<TrainingExample> parseTrainingExamples(String filename) throws IOException
	{
		List<TrainingExample> examples = new ArrayList<>();

		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		br.readLine();
		br.readLine();
		while((line = br.readLine()) != null)
		{
			TrainingExample e = new TrainingExample();
			String[] values = line.split("\\s+");
			for(int i = 0; i < values.length; i++)
			{
				if(i < inputValueCount)
				{
					e.input.add(Double.parseDouble(values[i]));
				}
				else
				{
					e.teacherValues.add(Double.parseDouble(values[i]));
				}
			}

			examples.add(e);
		}

		return examples;
	}

	private static double getGlobalError(MLP mlp, List<TrainingExample> trainingExamples)
	{
		double error = 0d;

		for(TrainingExample e : trainingExamples)
		{
			error += getErrorForSingleExample(mlp, e);
		}

		return error;
	}

	private static double getErrorForSingleExample(MLP mlp, TrainingExample example)
	{
		double error = 0d;
		mlp.forward(example.input);
		List<Double> results = mlp.getResults();
		for(int i = 0; i < results.size(); i++)
		{
			error += (example.teacherValues.get(i) - results.get(i))*(example.teacherValues.get(i) - results.get(i));
		}

		return error;
	}
}
