import java.util.List;

/**
 * Created by Tobias on 05.11.2017.
 */
public class MLP
{
	InputLayer inputLayer;
	List<Layer> layers;
	Layer lastLayer;

	public MLP(InputLayer inputLayer, List<Layer> layers)
	{
		this.inputLayer = inputLayer;
		this.layers = layers;

		for(Layer layer : layers)
		{
			lastLayer = layer;
		}
	}

	public void forward(List<Double> input)
	{
		if(inputLayer.getNeuronCount() != input.size())
			throw new IllegalArgumentException("Input value count does not match input neuron count.");

		inputLayer.setActivations(input);

		for(Layer layer : layers)
		{
			layer.forward(input);

			input = layer.getActivations();
		}
	}

	public void backprop(List<Double> teacherValues)
	{
		if(teacherValues.size() != lastLayer.getNeuronCount())
			throw new IllegalArgumentException("Teacher value count does not match output neuron count.");

		//BP for output layer
		layers.get(layers.size()-1).backpropLastLayer(teacherValues);

		for(int i = layers.size() - 2; i >= 0; i--)
		{
			layers.get(i).backpropHiddenLayer(layers.get(i+1));
		}
	}

	public void updateWeights()
	{
		AbstractLayer prevLayer = inputLayer;

		for(Layer layer : layers)
		{
			layer.updateWeights(prevLayer);

			prevLayer = layer;
		}
	}

	public List<Double> getResults()
	{
		return lastLayer.getActivations();
	}
}
