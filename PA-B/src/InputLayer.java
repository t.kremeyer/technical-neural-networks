import java.util.List;

/**
 * Created by Tobias on 05.11.2017.
 */
public class InputLayer implements AbstractLayer
{
	private int neuronCount;
	private List<Double> activations;

	public InputLayer(int neuronCount)
	{
		this.neuronCount = neuronCount;
	}

	@Override
	public int getNeuronCount()
	{
		return neuronCount;
	}

	public void setActivations(List<Double> activations)
	{
		this.activations = activations;
	}

	@Override
	public List<Double> getActivations()
	{
		return activations;
	}
}
