package TransferFunctions;

/**
 * Created by Tobias on 05.11.2017.
 */
public class TanhTransferFunction implements TransferFunction
{
	@Override
	public double calc(double input)
	{
		return Math.tanh(input);
	}

	@Override
	public double calcDerivativeWithOutputReuse(double output)
	{
		return 1.0 - output * output;
	}
}
