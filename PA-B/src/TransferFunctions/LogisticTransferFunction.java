package TransferFunctions;

/**
 * Created by Tobias on 05.11.2017.
 */
public class LogisticTransferFunction implements TransferFunction
{
	@Override
	public double calc(double input)
	{
		return 1.0/(1.0+Math.exp(-input));
	}

	@Override
	public double calcDerivativeWithOutputReuse(double output)
	{
		return output*(1.0-output);
	}

}
