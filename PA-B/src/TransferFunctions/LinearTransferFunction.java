package TransferFunctions;

/**
 * Created by Tobias on 05.11.2017.
 */
public class LinearTransferFunction implements TransferFunction
{
	@Override
	public double calc(double input) {
		return input;
	}

	@Override
	public double calcDerivativeWithOutputReuse(double output) {
		return 1;
	}
}
