package TransferFunctions;

/**
 * Created by Tobias on 05.11.2017.
 */
public interface TransferFunction
{
	public double calc(double input);
	public double calcDerivativeWithOutputReuse(double output);
}
