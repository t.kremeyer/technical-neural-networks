set terminal png size 1024,768
set output '34.png'
set ylabel '{/Symbol h}(t)'
set xlabel 't'
set xrange [0:40]
set grid
e_init = 0.9
e_final = 0.2
t_max = 30
f(x) = (e_init - e_final) * exp(-5 * log(2) * t_max**-1 * x) + e_final
set label "{/Symbol h}_{init}=0.9, {/Symbol h}_{final}=0.2, t_{max}=30" at 30,0.83
plot f(x) title '{/Symbol h}(t)' with lines lw 3