import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 20.11.2017.
 */
public class RBFLayer implements AbstractLayer
{
	private List<RBFNeuron> rbfNeurons;

	public RBFLayer(int rbfCount, List<List<Double>> centers, List<Double> gaussSizes)
	{
		rbfNeurons = new ArrayList<>();

		for(int i = 0; i < rbfCount; i++)
		{
			rbfNeurons.add(new RBFNeuron(centers.get(i), gaussSizes.get(i)));
		}
	}

	@Override
	public int getNeuronCount()
	{
		return rbfNeurons.size();
	}

	public void forward(List<Double> input)
	{
		for(RBFNeuron rbfNeuron : rbfNeurons)
		{
			rbfNeuron.forward(input);
		}
	}

	@Override
	public List<Double> getActivations()
	{
		List<Double> activations = new ArrayList<>();

		for(RBFNeuron rbfNeuron : rbfNeurons)
		{
			activations.add(rbfNeuron.getActivation());
		}

		return activations;
	}
}
