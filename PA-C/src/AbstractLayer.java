import java.util.List;

/**
 * Created by Tobias on 05.11.2017.
 */
public interface AbstractLayer
{
	public int getNeuronCount();

	public List<Double> getActivations();
}
