import TransferFunctions.TransferFunction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Tobias on 05.11.2017.
 */
public class Layer implements AbstractLayer
{
	private double learningRate;

	private List<Neuron> neurons;
	private List<Double> activations;
	private List<Double> deltas;

	private TransferFunction transferFct;
	private AbstractLayer previous;

	public Layer(int neuronCount, double learningRate, AbstractLayer previous, TransferFunction transferFct, RandomWeightGenerator gen)
	{
		this.learningRate = learningRate;

		this.transferFct = transferFct;
		this.previous = previous;

		//Create neurons
		neurons = new ArrayList<>();
		activations = new ArrayList<>();
		deltas = new ArrayList<>();

		for(int i = 0; i < neuronCount; i++)
		{
			//+1 for BIAS
			neurons.add(new Neuron(gen.nextWeights(previous.getNeuronCount()+1), transferFct));
			activations.add(0d);
			deltas.add(0d);
		}
	}

	public void forward(List<Double> input)
	{
		Iterator<Neuron> neuronIt = neurons.iterator();
		ListIterator<Double> activationIt = activations.listIterator();

		while(neuronIt.hasNext() && activationIt.hasNext())
		{
			Neuron neuron = neuronIt.next();
			Double activation = activationIt.next();

			neuron.forward(input);
			activationIt.set(neuron.getActivation());
		}
	}

	public void backpropLastLayer(List<Double> teacherValues)
	{
		if(neurons.size() != teacherValues.size())
			throw new IllegalArgumentException("Output Layer Neuron Count != Teacher vector size");

		for(int neuronNumber = 0; neuronNumber < neurons.size(); neuronNumber++)
		{
			neurons.get(neuronNumber).backpropLastLayer(teacherValues.get(neuronNumber));
			deltas.set(neuronNumber, neurons.get(neuronNumber).getDelta());
		}
	}

	public void backpropHiddenLayer(Layer nextLayer)
	{
		for(int neuronNumber = 0; neuronNumber < neurons.size(); neuronNumber++)
		{
			neurons.get(neuronNumber).backpropHiddenLayer(nextLayer.getDeltas(), nextLayer.getWeightsForPrevNeuron(neuronNumber));
			deltas.set(neuronNumber, neurons.get(neuronNumber).getDelta());
		}
	}

	public List<Double> getWeightsForPrevNeuron(int prevNeuronNumber)
	{
		List<Double> result = new ArrayList<>();

		for(Neuron neuron : neurons)
		{
			result.add(neuron.getWeights().get(prevNeuronNumber));
		}

		return result;
	}

	@Override
	public int getNeuronCount()
	{
		return neurons.size();
	}

	@Override
	public List<Double> getActivations()
	{
		return activations;
	}

	public List<Double> getDeltas()
	{
		return deltas;
	}

	public void updateWeights(AbstractLayer prevLayer)
	{
		for(Neuron neuron : neurons)
		{
			neuron.updateWeights(learningRate, prevLayer.getActivations());
		}
	}
}
