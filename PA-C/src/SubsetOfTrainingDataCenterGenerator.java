import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tobias on 19.11.2017.
 */
public class SubsetOfTrainingDataCenterGenerator
{
	private int centerCount;
	private List<TrainingExample> trainingExamples;
	private Random random;

	public SubsetOfTrainingDataCenterGenerator(int centerCount, List<TrainingExample> trainingExamples, long seed)
	{
		this.centerCount = centerCount;
		this.trainingExamples = trainingExamples;

		random = new Random(seed);
	}

	public List<List<Double>> generateCenters()
	{
		List<List<Double>> centers = new ArrayList<>();

		if(trainingExamples.size() > centerCount)
		{
			List<Integer> chosen = new ArrayList<>();
			for (int i = 0; i < centerCount; i++)
			{
				int next = random.nextInt(trainingExamples.size() - chosen.size());
				for (int chosenIndex : chosen)
				{
					if (chosenIndex < next) next++;
				}
				chosen.add(next);
				centers.add(trainingExamples.get(next).input);
			}
		}
		else //Trivial case: More centers than training examples => Choose every example (perhaps multiple times)
		{
			for(int i = 0; i < centerCount; i++)
			{
				centers.add(trainingExamples.get(i%trainingExamples.size()).input);
			}
		}

		return centers;
	}
}
