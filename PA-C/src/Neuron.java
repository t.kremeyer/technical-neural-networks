import TransferFunctions.TransferFunction;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Tobias on 05.11.2017.
 */
public class Neuron
{
	private TransferFunction transferFct;

	//LAST weight is BIAS
	private List<Double> weights;

	private double netInput;
	private double activation;
	private double delta;

	public Neuron(List<Double> initWeights, TransferFunction transferFct)
	{
		this.transferFct = transferFct;

		weights = initWeights;
	}

	public void forward(List<Double> input)
	{
		//+1 for BIAS
		if(input.size()+1 != weights.size())
			throw new IllegalArgumentException("Intermediate out-/input value count + Bias does not match weight count (Internal)");

		Iterator<Double> inputIt = input.iterator();
		Iterator<Double> weightIt = weights.iterator();

		netInput = 0;

		while(inputIt.hasNext() && weightIt.hasNext())
		{
			double inputValue = inputIt.next();
			double weight = weightIt.next();

			netInput += inputValue * weight;
		}

		//BIAS
		double weight = weightIt.next();
		netInput += weight;

		activation = transferFct.calc(netInput);
	}

	public void backpropLastLayer(double teacher)
	{
		delta = transferFct.calcDerivativeWithOutputReuse(activation)*(teacher-activation);
	}

	public void backpropHiddenLayer(List<Double> deltas, List<Double> outputWeights)
	{
		if(deltas.size() != outputWeights.size())
			throw new IllegalArgumentException("Delta count != output weight count in Backprop for hidden layer");

		delta = 0;

		for(int i = 0; i < deltas.size(); i++)
		{
			delta += deltas.get(i) * outputWeights.get(i);
		}

		delta *= transferFct.calcDerivativeWithOutputReuse(activation);
	}

	public void updateWeights(double learningRate, List<Double> prevActivations)
	{
		if(prevActivations.size()+1 != weights.size())
			throw new IllegalArgumentException("Prev activations vector size != Input weight count on update weights");

		for(int weightNumber = 0; weightNumber < prevActivations.size(); weightNumber++)
		{
			weights.set(weightNumber,
					weights.get(weightNumber) + (learningRate * prevActivations.get(weightNumber) * delta));
		}

		//BIAS
		weights.set(weights.size()-1,
				weights.get(weights.size()-1) + (learningRate * 1 * delta));
	}

	public double getNetInput()
	{
		return netInput;
	}

	public double getActivation()
	{
		return activation;
	}

	public List<Double> getWeights()
	{
		return weights;
	}

	public double getDelta()
	{
		return delta;
	}
}
