import TransferFunctions.LinearTransferFunction;
import TransferFunctions.LogisticTransferFunction;
import TransferFunctions.TanhTransferFunction;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Tobias on 05.11.2017.
 */
public class Main
{
	static int epochCount = 1000;

	static int inputCount = 2;
	static int rbfCount = 10;
	static int outputCount = 1;

	static TanhTransferFunction tanh = new TanhTransferFunction();
	static LogisticTransferFunction logistic = new LogisticTransferFunction();
	static LinearTransferFunction linear = new LinearTransferFunction();

	static RandomWeightGenerator randomWeightGenerator = new RandomWeightGenerator(1234567, -0.5, 0.5);
	static Random shuffleNumberGenerator = new Random(1234532);

	public static void main(String[] args) throws IOException
	{
		if(args.length < 2)
		{
			System.err.println("Usage: [Path to input training data file] [Path to input test data file]");
			System.exit(1);
		}

		List<TrainingExample> trainingExamples = parseTrainingExamples(args[0]);
		List<TrainingExample> testExamples = parseTrainingExamples(args[1]);

		SubsetOfTrainingDataCenterGenerator centerGenerator = new SubsetOfTrainingDataCenterGenerator(rbfCount, trainingExamples, 87253);

		//Default gauss size 1.0
		List<Double> gaussSizes = new ArrayList<>();
		for(int i = 0; i < rbfCount; i++)
		{
			gaussSizes.add(1.0);
		}

		RBFNetwork rbf = new RBFNetwork(inputCount, rbfCount, outputCount, linear, centerGenerator.generateCenters(), gaussSizes, randomWeightGenerator, 0.1);

		Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("out/learning.curve"), "utf-8"));

		Collections.shuffle(trainingExamples, shuffleNumberGenerator);

		System.out.println("Training examples: ");
		System.out.println(trainingExamples);
		System.out.println("Test examples: ");
		System.out.println(testExamples);


		for(int i = 0; i < epochCount; i++)
		{
			writer.write(i + " " + Double.toString(getGlobalError(rbf, trainingExamples)) + " "
					+ Double.toString(getGlobalError(rbf, testExamples)) + "\n");
			for(TrainingExample example : trainingExamples)
			{
				rbf.forward(example.input);
				rbf.backprop(example.teacherValues);
				rbf.updateWeights();
			}
		}

		writer.close();

	}

	private static List<TrainingExample> parseTrainingExamples(String filename) throws IOException
	{
		List<TrainingExample> examples = new ArrayList<>();

		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		br.readLine();
		br.readLine();
		while((line = br.readLine()) != null)
		{
			TrainingExample e = new TrainingExample();
			String[] values = line.split("\\s+");
			for(int i = 0; i < values.length; i++)
			{
				if(i < inputCount)
				{
					e.input.add(Double.parseDouble(values[i]));
				}
				else
				{
					e.teacherValues.add(Double.parseDouble(values[i]));
				}
			}

			examples.add(e);
		}

		return examples;
	}

	private static double getGlobalError(RBFNetwork rbf, List<TrainingExample> trainingExamples)
	{
		double error = 0d;

		for(TrainingExample e : trainingExamples)
		{
			error += getErrorForSingleExample(rbf, e);
		}

		return error;
	}

	private static double getErrorForSingleExample(RBFNetwork rbf, TrainingExample example)
	{
		double error = 0d;
		rbf.forward(example.input);
		List<Double> results = rbf.getResults();
		for(int i = 0; i < results.size(); i++)
		{
			error += (example.teacherValues.get(i) - results.get(i))*(example.teacherValues.get(i) - results.get(i));
		}

		return error;
	}
}
