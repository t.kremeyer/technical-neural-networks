import java.util.List;

/**
 * Created by Tobias on 19.11.2017.
 */
public class RBFNeuron
{
	private List<Double> center;
	private double gaussSize;
	private double activation;

	public RBFNeuron(List<Double> center, double gaussSize)
	{
		this.center = center;
		this.gaussSize = gaussSize;
	}

	public void forward(List<Double> input)
	{
		activation = 0;
		for(int i = 0; i < input.size(); i++)
		{
			activation += Math.pow(input.get(i) - center.get(i), 2);
		}
		activation = Math.sqrt(activation);
		activation = gauss(activation, gaussSize);
	}

	public static double gauss(double x, double size)
	{
		return Math.exp(-x*x / (2 * size * size));
	}

	public double getActivation()
	{
		return activation;
	}
}
