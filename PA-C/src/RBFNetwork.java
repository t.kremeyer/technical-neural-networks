import TransferFunctions.TransferFunction;

import java.util.List;

/**
 * Created by Tobias on 19.11.2017.
 */
public class RBFNetwork
{
	private int inputCount;
	private RBFLayer rbfLayer;
	private Layer outputLayer;

	public RBFNetwork(int inputCount, int rbfCount, int outputCount, TransferFunction outputTransferFunction, List<List<Double>> centers, List<Double> gaussSizes, RandomWeightGenerator randomWeightGenerator, double learningRate)
	{
		this.inputCount = inputCount;

		rbfLayer = new RBFLayer(rbfCount, centers, gaussSizes);

		outputLayer = new Layer(outputCount, learningRate, rbfLayer, outputTransferFunction, randomWeightGenerator);
	}

	public void forward(List<Double> input)
	{
		rbfLayer.forward(input);
		outputLayer.forward(rbfLayer.getActivations());
	}

	public void backprop(List<Double> teacherValues)
	{
		outputLayer.backpropLastLayer(teacherValues);
	}

	public void updateWeights()
	{
		outputLayer.updateWeights(rbfLayer);
	}

	public List<Double> getResults()
	{
		return outputLayer.getActivations();
	}
}
