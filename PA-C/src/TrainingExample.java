import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 05.11.2017.
 */
public class TrainingExample
{
	public List<Double> input = new ArrayList<>();
	public List<Double> teacherValues = new ArrayList<>();
	public String toString()
	{
		return "INPUT: " + input.toString() + " OUTPUT: " + teacherValues.toString();
	}
}
