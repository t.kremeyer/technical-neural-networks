set terminal png
set output 'learning.png'
set ylabel 'error'
set xlabel 'epochs'
set grid
plot 'learning.curve' using 1:2 lc rgb 'red' with lines title 'Training', \
'learning.curve' using 1:3 lc rgb 'blue' with lines title 'Test'
