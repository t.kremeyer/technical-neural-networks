# Programming Assignment C

## Usage

Compile the project and start it using class `Main` as entry point.  
The program requires following options in the following order:  
* Path to the training examples input file
* Path to the test examples input file

Other options such as neuron count, learning rate and transfer functions may be customized by changing them in the code.

The program will output a file to `[Working Dir]/out/learning.curve` with the learning curve; per line there are two values, separated by a space: First the training error, second the test error.