#!/bin/bash

mkdir -p output/png

cowsay "Plotting training process ..."

for FILE in output/csv/*.csv
do
	NAME=${FILE##*/}
	BASE=${NAME%.csv}
	
	gnuplot -c plot_winners.gnuplot $BASE
done