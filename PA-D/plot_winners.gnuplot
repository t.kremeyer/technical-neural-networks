set terminal png
set output 'output/png/$0.png'
set grid
plot 'input/train_3_clusters.dat' using 1:2 lc rgb 'red' with points title 'Training Data', \
'output/csv/$0.csv' using 1:2 lc rgb 'blue' pt 71 lw 2 with linespoints title 'Centers'
