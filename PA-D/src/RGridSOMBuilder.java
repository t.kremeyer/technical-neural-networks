import LearningRate.*;
import NeighbourhoodFunction.*;
import SOM.RectangularGridDistanceFunctions.EuclideanDistance;
import SOM.RectangularGridDistanceFunctions.RectGridDistanceFunction;
import SOM.RectangularGridSOM;

import java.util.*;

/**
 * Created by Tobias on 03.12.2017.
 */
public class RGridSOMBuilder
{
	private NeighbourhoodFunction neighbourhoodFct = new GaussianNeighbourhoodFunction(1);
	private RectGridDistanceFunction distanceFct = new EuclideanDistance();

	private double initLearningRate = 0.8;
	private double convergeLearningRate = 0.2;
	private double lambda = 0.00034;

	private Integer[] edgeLengths;

	private List<List<Double>> initCenters;

	public RGridSOMBuilder setNeighbourhoodFct(NeighbourhoodFunction neighbourhoodFct)
	{
		this.neighbourhoodFct = neighbourhoodFct;
		return this;
	}

	public RGridSOMBuilder setDistanceFct(RectGridDistanceFunction distanceFct)
	{
		this.distanceFct = distanceFct;
		return this;
	}


	public RGridSOMBuilder setInitLearningRate(double initLearningRate)
	{
		this.initLearningRate = initLearningRate;
		return this;
	}

	public RGridSOMBuilder setConvergeLearningRate(double convergeLearningRate)
	{
		this.convergeLearningRate = convergeLearningRate;
		return this;
	}

	public RGridSOMBuilder setLambda(double lambda)
	{
		this.lambda = lambda;
		return this;
	}

	public RGridSOMBuilder setEdgeLengths(Integer ... edgeLengths)
	{
		this.edgeLengths = edgeLengths;
		return this;
	}

	public RGridSOMBuilder selectInitCentersFromData(List<List<Double>> examples, Random random)
	{
		Collections.shuffle(examples);

		initCenters = new ArrayList<>();

		int neuronCount = 1;

		for(int edgeLength : edgeLengths)
		{
			neuronCount *= edgeLength;
		}

		for(int i = 0; i < neuronCount; i++)
		{
			List<Double> center = new ArrayList<>();

			center.addAll(examples.get(i%examples.size()));

			initCenters.add(center);
		}

		return this;
	}

	public RectangularGridSOM buildSOM()
	{
		return new RectangularGridSOM(Arrays.asList(edgeLengths), distanceFct, neighbourhoodFct,
				new ExponentialDecayLearningRate(initLearningRate,convergeLearningRate, lambda),
				initCenters);
	}

}
