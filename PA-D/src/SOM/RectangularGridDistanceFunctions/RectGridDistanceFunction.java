package SOM.RectangularGridDistanceFunctions;

import java.util.List;

/**
 * Created by Tobias on 26.11.2017.
 */
public interface RectGridDistanceFunction
{
	double calc(List<Integer> pos1, List<Integer> pos2);
}
