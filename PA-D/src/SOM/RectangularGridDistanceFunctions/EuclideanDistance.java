package SOM.RectangularGridDistanceFunctions;

import java.util.List;

/**
 * Created by Tobias on 26.11.2017.
 */
public class EuclideanDistance implements RectGridDistanceFunction
{
	@Override
	public double calc(List<Integer> pos1, List<Integer> pos2)
	{
		if(pos1.size() != pos2.size())
			throw new IllegalArgumentException("Inconsistent dimensions.");

		double dist = 0;
		for(int i = 0; i < pos1.size(); i++)
		{
			dist += (Math.pow(pos1.get(i) - pos2.get(i), 2));
		}
		return Math.sqrt(dist);
	}
}
