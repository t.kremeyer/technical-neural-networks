package SOM;

import java.util.List;

/**
 * Created by Tobias on 30.11.2017.
 */
public class SOMNeuron
{
	private List<Double> center;
	private List<Double> input;
	private double distance;

	public SOMNeuron(List<Double> initCenter)
	{
		center = initCenter;
	}

	public double present(List<Double> example)
	{
		if(example.size() != center.size())
			throw new IllegalArgumentException("Example dimension != Center dimension");

		this.input = example;
		distance = 0;
		for(int i = 0; i < center.size(); i++)
		{
			distance += (Math.pow(input.get(i) - center.get(i), 2));
		}
		distance = Math.sqrt(distance);
		return distance;
	}

	public void adjust(double learningRate, double neighbourhood)
	{
		for(int i = 0; i < center.size(); i++)
		{
			double delta = learningRate * neighbourhood * (input.get(i) - center.get(i));
			center.set(i, center.get(i) + delta);
		}
	}

	public List<Double> getCenter()
	{
		return center;
	}
}
