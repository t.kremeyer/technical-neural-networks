package SOM;

import SOM.RectangularGridDistanceFunctions.RectGridDistanceFunction;
import LearningRate.LearningRate;
import NeighbourhoodFunction.NeighbourhoodFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 26.11.2017.
 */
public class RectangularGridSOM implements SOM
{
	private RectGridDistanceFunction gridDistanceFunction;
	private NeighbourhoodFunction neighbourhoodFunction;
	private LearningRate learningRate;
	//Index: b-adic representation of the position vector: (1,2,3) -> 1+2*edgeLengths[0]+3*edgeLengths[0]*edgeLengths[1]
	private List<Integer> edgeLenghts;
	private List<SOMNeuron> neurons;
	private double minDistance;
	private int winnerId;

	public RectangularGridSOM(List<Integer> edgeLengths, RectGridDistanceFunction gridDistanceFunction, NeighbourhoodFunction neighbourhoodFunction, LearningRate learningRate, List<List<Double>> initCenters)
	{
		this.edgeLenghts = edgeLengths;
		this.gridDistanceFunction = gridDistanceFunction;
		this.neighbourhoodFunction = neighbourhoodFunction;
		this.learningRate = learningRate;
		neurons = new ArrayList<>();
		for(List<Double> initCenter : initCenters)
		{
			neurons.add(new SOMNeuron(initCenter));
		}
	}

	@Override
	public void present(List<Double> example)
	{
		minDistance = Double.MAX_VALUE;
		for(int i = 0; i < neurons.size(); i++)
		{
			SOMNeuron neuron = neurons.get(i);

			double d = neuron.present(example);
			if(d < minDistance)
			{
				minDistance = d;
				winnerId = i;
			}
		}
	}

	@Override
	public double getMinDistance()
	{
		return minDistance;
	}

	public List<List<Double>> getCenters()
	{
		List<List<Double>> centers = new ArrayList<>();

		for(SOMNeuron n : neurons)
		{
			centers.add(n.getCenter());
		}

		return centers;
	}

	@Override
	public void adjust()
	{
		double lr = learningRate.getLearningRate();

		for(int i = 0; i < neurons.size(); i++)
		{
			SOMNeuron neuron = neurons.get(i);

			double gridDistance = gridDistanceFunction.calc(listPosToGridPos(winnerId, edgeLenghts), listPosToGridPos(i, edgeLenghts));
			neuron.adjust(lr, neighbourhoodFunction.calc(gridDistance));
		}
	}

	public SOMNeuron getNeuronByGridPos(List<Integer> pos)
	{
		return neurons.get(gridPosToListPos(pos, edgeLenghts));
	}

	@Override
	public List<List<SOMNeuron>> getAdjacentNeuronPairs()
	{
		List<List<SOMNeuron>> pairs = new ArrayList<>();

		for(int neuronId = 0; neuronId < neurons.size(); neuronId++)
		{
			List<Integer> neuronPos = listPosToGridPos(neuronId, edgeLenghts);

			for(int i = 0; i < neuronPos.size(); i++)
			{
				if(neuronPos.get(i) != 0 && edgeLenghts.get(i) > 1)
				{
					List<SOMNeuron> adj = new ArrayList<>();
					neuronPos.set(i, neuronPos.get(i)-1);
					adj.add(neurons.get(neuronId));
					adj.add(neurons.get(gridPosToListPos(neuronPos, edgeLenghts)));
					neuronPos.set(i, neuronPos.get(i)+1);

					pairs.add(adj);
				}

				if(neuronPos.get(i) < edgeLenghts.get(i)-1)
				{
					List<SOMNeuron> adj = new ArrayList<>();
					neuronPos.set(i, neuronPos.get(i)+1);
					adj.add(neurons.get(neuronId));
					adj.add(neurons.get(gridPosToListPos(neuronPos, edgeLenghts)));
					neuronPos.set(i, neuronPos.get(i)-1);

					pairs.add(adj);
				}
			}
		}

		return pairs;
	}

	//vector to b-adic representation
	private static int gridPosToListPos(List<Integer> pos, List<Integer> edgeLenghts)
	{
		if(pos.size() != edgeLenghts.size())
		{
			throw new IllegalArgumentException("Position vector size != Grid dimension");
		}

		double factor = 1;
		int listPos = 0;

		for(int i = 0; i < pos.size(); i++)
		{
			listPos += factor * pos.get(i);
			factor *= edgeLenghts.get(i);
		}

		return listPos;
	}

	//b-adic representation to vector
	private static List<Integer> listPosToGridPos(int listPos, List<Integer> edgeLenghts)
	{
		List<Integer> gridPos = new ArrayList<>(edgeLenghts.size());
		for(int i = 0; i < edgeLenghts.size(); i++)
		{
			gridPos.add(listPos % edgeLenghts.get(i));
			listPos -= gridPos.get(i);
			listPos /= edgeLenghts.get(i);
		}
		return gridPos;
	}
}
