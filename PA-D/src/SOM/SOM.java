package SOM;

import java.util.List;

/**
 * Created by Tobias on 03.12.2017.
 */
public interface SOM
{
	void present(List<Double> example);

	double getMinDistance();

	void adjust();

	List<List<SOMNeuron>> getAdjacentNeuronPairs();

}
