package NeighbourhoodFunction;

/**
 * Created by Tobias on 26.11.2017.
 */
public interface NeighbourhoodFunction
{
	public double calc(double input);
}
