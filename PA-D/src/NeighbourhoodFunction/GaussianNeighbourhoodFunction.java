package NeighbourhoodFunction;

/**
 * Created by Tobias on 26.11.2017.
 */
public class GaussianNeighbourhoodFunction implements NeighbourhoodFunction
{
	private double size_param;

	public GaussianNeighbourhoodFunction(double neighbourhoodSize)
	{
		size_param = neighbourhoodSize;
	}

	@Override
	public double calc(double input)
	{
		return Math.exp(-0.5 * input * input / (size_param * size_param));
	}
}
