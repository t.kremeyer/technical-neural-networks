package LearningRate;

/**
 * Created by Tobias on 26.11.2017.
 */
public class ExponentialDecayLearningRate implements LearningRate
{
	private double init;
	private double converge;
	private double lambda;
	private int time;

	public ExponentialDecayLearningRate(double initLearningRate, double convergeLearningRate, double lambda)
	{
		time = 0;
		init = initLearningRate;
		converge = convergeLearningRate;
		this.lambda = lambda;
	}

	@Override
	public double getLearningRate()
	{
		double lr = (init - converge) * Math.exp(0-lambda * time) + converge;
		time++;
		return lr;
	}
}
