package LearningRate;

/**
 * Created by Tobias on 26.11.2017.
 */
public interface LearningRate
{
	double getLearningRate();
}
