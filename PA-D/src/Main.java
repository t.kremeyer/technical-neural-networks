import SOM.RectangularGridDistanceFunctions.*;
import NeighbourhoodFunction.*;
import SOM.*;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Tobias on 26.11.2017.
 */
public class Main
{
	static double neighbourhoodSize = 0.5;

	static Random shuffleRnd = new Random(32947619);

	public static void main(String[] args) throws IOException {
		RectGridDistanceFunction gridDistanceFunction = new EuclideanDistance();

		NeighbourhoodFunction neighbourhoodFunction = new GaussianNeighbourhoodFunction(neighbourhoodSize);

		List<List<Double>> trainingExamples = parseTrainingExamples(args[0]);

		RectangularGridSOM som1 = new RGridSOMBuilder()
				.setEdgeLengths(2, 2)
				.setInitLearningRate(0.8)
				.setConvergeLearningRate(0.2)
				.setLambda(0.00034)
				.setDistanceFct(gridDistanceFunction)
				.setNeighbourhoodFct(neighbourhoodFunction)
				.selectInitCentersFromData(trainingExamples, shuffleRnd)
				.buildSOM();

		RectangularGridSOM som2 = new RGridSOMBuilder()
				.setEdgeLengths(2, 2)
				.setInitLearningRate(0.8)
				.setConvergeLearningRate(0.2)
				.setLambda(0.00034)
				.setDistanceFct(gridDistanceFunction)
				.setNeighbourhoodFct(neighbourhoodFunction)
				.selectInitCentersFromData(trainingExamples, shuffleRnd)
				.buildSOM();

		RectangularGridSOM som3 = new RGridSOMBuilder()
				.setEdgeLengths(2, 2)
				.setInitLearningRate(0.8)
				.setConvergeLearningRate(0.2)
				.setLambda(0.00034)
				.setDistanceFct(gridDistanceFunction)
				.setNeighbourhoodFct(neighbourhoodFunction)
				.selectInitCentersFromData(trainingExamples, shuffleRnd)
				.buildSOM();

		RectangularGridSOM som4 = new RGridSOMBuilder()
				.setEdgeLengths(2, 2)
				.setInitLearningRate(0.8)
				.setConvergeLearningRate(0.2)
				.setLambda(0.00034)
				.setDistanceFct(gridDistanceFunction)
				.setNeighbourhoodFct(neighbourhoodFunction)
				.selectInitCentersFromData(trainingExamples, shuffleRnd)
				.buildSOM();

		MSOM msom = new MSOM(som1, som2, som3, som4);

		for(int i = 0; i < 1000; i++)
		{
			for(List<Double> example : trainingExamples)
			{
				msom.present(example);
				msom.adjust();
			}

			if(i < 20 || i % 20 == 0) printPairsToFile(msom, Paths.get(args[1], "centers_" + i + ".csv").toString());

			Collections.shuffle(trainingExamples, shuffleRnd);
		}

		printPairsToFile(msom, Paths.get(args[1], "centers.csv").toString());
	}

	private static List<List<Double>> parseTrainingExamples(String filename) throws IOException
	{
		List<List<Double>> examples = new ArrayList<>();

		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		br.readLine();
		br.readLine();
		while((line = br.readLine()) != null)
		{
			List<Double> e = new ArrayList<>();
			String[] values = line.split("\\s+");
			for(String value : values)
			{
				e.add(Double.parseDouble(value));
			}

			examples.add(e);
		}

		return examples;
	}

	private static void printPairsToFile(SOM som, String filename) throws IOException
	{
		PrintWriter wr = new PrintWriter(filename, "UTF-8");

		for(List<SOMNeuron> pair : som.getAdjacentNeuronPairs())
		{
			for(SOMNeuron n : pair)
			{
				for(double c : n.getCenter())
				{
					wr.print(c + "\t");
				}
				wr.print("\n");
			}
			wr.print("\n");
		}

		wr.close();
	}

}
