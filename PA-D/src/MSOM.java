import SOM.SOM;
import SOM.SOMNeuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tobias on 03.12.2017.
 */
public class MSOM implements SOM
{
	private SOM[] soms;
	private SOM winnerSOM;
	private double minDistance;

	public MSOM(SOM ... soms)
	{
		this.soms = soms;
	}

	@Override
	public void present(List<Double> example)
	{
		minDistance = Double.MAX_VALUE;

		for(SOM som : soms)
		{
			som.present(example);
			if(som.getMinDistance() < minDistance)
			{
				minDistance = som.getMinDistance();
				winnerSOM = som;
			}
		}
	}

	@Override
	public double getMinDistance()
	{
		return minDistance;
	}

	@Override
	public void adjust()
	{
		winnerSOM.adjust();
	}

	@Override
	public List<List<SOMNeuron>> getAdjacentNeuronPairs()
	{
		List<List<SOMNeuron>> pairs = new ArrayList<>();

		for(SOM som : soms)
		{
			pairs.addAll(som.getAdjacentNeuronPairs());
		}

		return pairs;
	}
}
