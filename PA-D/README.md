# Programming Assignment D: M-SOM

## Usage

Compile the project and start it using class `Main` as entry point.  
The program requires following options in the following order:  
* Path to the training examples input file (use `input/train_3_clusters.dat`)
* Path to the csv output folder (use `output/csv/`)

Other options such as neuron count, layer count, learning rate and transfer functions may be customized by changing them in the code.

The program will output some csv files `centers_{epoch}.csv` and at the end a file `centers.csv` with the training progress to the specified output folder; the format is as follows:
```csv
x1 y1
x2 y2

x3 y3
x4 y4

x5 y5
x6 y6
```

This file indicates that there are edges between (x1,y1)<->(x2,y2); (x3,y3)<->(x4,y4); (x5,y5)<->(x6,y6).

## Gnuplot

The shell script `plot.sh` plots all the output files from `output/csv/` to png files into `output/png/` using gnuplot.

## Input data

I used the file `train_3_clusters.dat` as input data. It contains points from 3 rectangular clusters:
- (0.0, 0.1) to (0.2, 0.2)
- (0.7, 0.7) to (0.9, 0.9)
- (0.1, 0.5) to (0.2, 0.6)

Parameters used:
- 4 SOMs, each 2x2 centers
- Learning Rate from 0.8 to 0.2
- Exponential decay: 0.6*exp(-lambda*t)+0.2 with lambda = 0.00034
- Euclidean Grid Distance
- Gaussian Neighbourhood Fct, size parameter: 0.5
- Initialize centers from random data points